from typing import List, Union, Optional

from fastapi import APIRouter, status, Depends
from starlette.responses import Response

from book_backend.models import StatusType
from book_backend.schemas.book import (
    Book,
    BookCreate,
    BookQueueCreate,
    BookQueue,
    BookQueueUpdate,
    BookInstance,
    BookWithQueue,
)
from book_backend.schemas.user import User
from book_backend.services.book import BookService
from book_backend.services.user import UserService, get_current_user

router = APIRouter(prefix="/book", tags=["books"])


@router.get("", response_model=Book)
def get_book_type(
    isbn: Optional[str] = None,
    book_id: Optional[int] = None,
    service: BookService = Depends(),
):
    if isbn:
        return service.get_book_by_isbn(isbn)
    if book_id:
        return service.get_book_by_id(book_id)


@router.get("/", response_model=List[Book])
def get_list_of_book_types(
    available: Optional[bool] = None,
    service: BookService = Depends(),
) -> List[Book]:
    """
    Get list of all book types or book type
    """
    if available:
        return service.get_available_book_types()

    return service.get_all_book_types()


@router.get("/{isbn}", response_model=Book)
def get_book(isbn: str, service: BookService = Depends()) -> Book:
    return service.get_book_by_isbn(isbn)


@router.post("/", response_model=Book, status_code=status.HTTP_201_CREATED)
def create_book_type(book_data: BookCreate, service: BookService = Depends()):
    """
    Creates new book type - not instance
    """
    return service.create_new_book_type(book_data)


@router.post("/line_up", response_model=BookQueue, status_code=status.HTTP_201_CREATED)
def get_in_line(queue_data: BookQueueCreate, service: BookService = Depends()):
    """
    Creates a place in queue if all book instances are busy
    """
    return service.add_to_end_of_book_queue(queue_data)


@router.post(
    "/{book_id}/borrow",
    response_model=BookWithQueue,
    status_code=status.HTTP_201_CREATED,
)
def borrow_book(
    book_id: int,
    user: User = Depends(get_current_user),
    service: BookService = Depends(),
):
    """
    Borrow available instance of book or get in line to get a book.
    Response contains only one of model: book instance if founded available
    or list of users in queue
    """
    return service.proceed_borrowing_book(book_type_id=book_id, user=user)
