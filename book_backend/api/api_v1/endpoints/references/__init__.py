from fastapi import APIRouter

from . import book


router = APIRouter(
    prefix="/references",
    tags=["references"],
)

router.include_router(book.router)
