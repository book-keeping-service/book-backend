from typing import Optional, List
from fastapi import APIRouter, Depends, status

from book_backend.schemas.user import UserCreate, User
from book_backend.services.user import UserService


router = APIRouter(prefix="/users", tags=["users"])


@router.get("", response_model=User)
async def get_user(
    user_id: Optional[int] = None,
    telegram_uid: Optional[str] = None,
    service: UserService = Depends(),
):
    if telegram_uid:
        return service.get_by_telegram_uid(telegram_uid)

    return service.get_by_user_id(user_id)


@router.get("/", response_model=List[User])
async def get_all_users(service: UserService = Depends()):
    return service.get_all_users()


@router.post("/", response_model=User, status_code=status.HTTP_201_CREATED)
async def create_user(user_data: UserCreate, service: UserService = Depends()):
    return service.register_new_user(user_data)
