from typing import List, Optional
from xmlrpc.client import boolean
from fastapi import APIRouter, Depends, status, Response

from book_backend.schemas.book import (
    BookInstance,
    BookInstanceCreate,
)
from book_backend.schemas.user import User
from book_backend.services.book import BookService

router = APIRouter(
    prefix="/books",
    tags=["books"],
)


@router.get("/", response_model=List[BookInstance])
def get_book_list(
    available: boolean = False,
    telegram_uid: Optional[str] = None,
    service: BookService = Depends(),
) -> List[BookInstance]:
    if available:
        return service.get_available_books()

    if telegram_uid:
        return service.get_books_by_telegram_user(telegram_uid)

    return service.get_all_books()


@router.get("/{book_id}", response_model=BookInstance)
async def get_book_detail(book_id: int, service: BookService = Depends()):
    return service.get_book_instance_info_by_id(book_id)


@router.delete(
    "/{book_id}",
    response_model=User,
    responses={status.HTTP_204_NO_CONTENT: {"model": None}},
)
def remove_book_instance(book_id: int, service: BookService = Depends()):
    """
    Remove existing book instance and return user who has this book or null
    """
    borrowed_user = service.remove_book_instance(book_id)
    if not borrowed_user:
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    return borrowed_user


@router.post(
    "/",
    response_model=BookInstance,
    status_code=status.HTTP_201_CREATED,
)
def create_new_book(
    book_data: BookInstanceCreate,
    service: BookService = Depends(),
) -> BookInstance:
    """
    Create new book instance
    """
    return service.add_new_book(book_data)


@router.patch("/{book_id}", response_model=BookInstance)
def update_book(
    book_id: int,
    service: BookService = Depends(),
) -> BookInstance:
    """
    Return book to the owner or give it to the first user in queue
    """
    return service.proceed_returning_book(book_id)
