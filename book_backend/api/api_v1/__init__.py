from fastapi import APIRouter

from .endpoints import user, book, references


router = APIRouter()
router.include_router(user.router)
router.include_router(book.router)
router.include_router(references.router)
