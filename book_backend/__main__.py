import uvicorn

from .settings import settings


uvicorn.run(
    app="book_backend.main:app",
    host=settings.server_host,
    port=settings.server_port,
    reload=True,
)


def start():
    uvicorn.run(
        app="book_backend.main:app",
        host=settings.server_host,
        port=settings.server_port,
    )
