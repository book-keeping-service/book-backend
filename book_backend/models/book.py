import enum
import sqlalchemy as sa
from sqlalchemy import UniqueConstraint, CheckConstraint
from sqlalchemy.orm import relationship

from ..database.base_class import Base


class StatusType(str, enum.Enum):
    available = "Доступна"
    loaned = "Взята в аренду"


class Book(Base):
    """
    Type of book
    """

    name = sa.Column(sa.String(128), nullable=False)
    author = sa.Column(sa.String(128), nullable=False)
    isbn = sa.Column(sa.String(13), nullable=False, unique=True)
    description = sa.Column(sa.Text, nullable=True)
    qty = sa.Column(sa.Integer, server_default="0", nullable=False)

    book_instances = relationship("BookInstance", back_populates="book")
    queues = relationship("BookQueue", back_populates="book")

    __table_args__ = (CheckConstraint(qty > 0, name="ck_book_positive_qty"),)


class BookInstance(Base):
    """
    Book instance to store info about each instance of book
    """

    status = sa.Column(
        sa.Enum(StatusType), default=StatusType.available, nullable=False
    )
    owner_id = sa.Column(sa.Integer, sa.ForeignKey("user.id"), nullable=False)
    loaned_by = sa.Column(sa.Integer, sa.ForeignKey("user.id"), nullable=True)
    book_id = sa.Column(sa.Integer, sa.ForeignKey("book.id"), nullable=False)

    owner = relationship("User", back_populates="book_owned", foreign_keys=[owner_id])
    borrower = relationship(
        "User", back_populates="book_loaned", foreign_keys=[loaned_by]
    )
    book = relationship("Book", back_populates="book_instances")


class BookQueue(Base):
    """
    Queue record for each book type
    """

    book_id = sa.Column(sa.Integer, sa.ForeignKey("book.id"), nullable=False)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.id"), nullable=False)
    prev_id = sa.Column(sa.Integer, sa.ForeignKey("bookqueue.id"), nullable=True)
    next_id = sa.Column(sa.Integer, sa.ForeignKey("bookqueue.id"), nullable=True)

    book = relationship("Book", back_populates="queues")

    __table_args__ = (UniqueConstraint("prev_id", "next_id"),)
