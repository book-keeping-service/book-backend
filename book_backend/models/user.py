import sqlalchemy as sa
from sqlalchemy.orm import relationship

from ..database.base_class import Base


class User(Base):
    """
    Модель пользователя
    """

    name = sa.Column(sa.String(128), nullable=False)
    telegram_uid = sa.Column(sa.String, unique=True)

    book_owned = relationship(
        "BookInstance", back_populates="owner", foreign_keys="BookInstance.owner_id"
    )
    book_loaned = relationship(
        "BookInstance",
        back_populates="borrower",
        foreign_keys="BookInstance.loaned_by",
    )
