from typing import List, Optional, Union, Dict, Any

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from .base import CRUDBase
from ..models import User
from ..models.book import Book, BookInstance, StatusType, BookQueue
from ..schemas.book import (
    BookCreate,
    BookInstanceCreate,
    BookInstanceUpdate,
    BookUpdate,
    BookQueueCreate,
    BookQueueUpdate,
    BookInstance as BookInstanceSchema,
)


class CRUDBook(CRUDBase[Book, BookCreate, BookUpdate]):
    def get_by_isbn(self, db: Session, isbn: str) -> Book:
        return db.query(self.model).where(self.model.isbn == isbn).first()

    def get_available_multi(self, db: Session) -> List[Book]:
        return db.query(self.model).where(self.model.qty > 0).all()


class CRUDBookInstance(CRUDBase[BookInstance, BookInstanceCreate, BookInstanceUpdate]):
    def get_available_books(
        self, db: Session, *, skip: int = 0, limit: int = 100
    ) -> List[BookInstance]:
        return (
            db.query(self.model)
            .where(self.model.status == StatusType.available)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def get_books_by_user(self, db: Session, user_id: int):
        return (
            db.query(self.model)
            .where((self.model.loaned_by == user_id) | (self.model.owner_id == user_id))
            .all()
        )

    def create(self, db: Session, *, obj_in: BookInstanceCreate) -> BookInstance:
        book_db: BookInstance = super().create(db, obj_in=obj_in)

        book_db.book.qty += 1
        db.add(book_db)
        db.commit()

        return book_db

    def update(
        self,
        db: Session,
        *,
        db_obj: BookInstance,
        obj_in: Union[BookInstanceUpdate, Dict[str, Any]]
    ) -> BookInstance:
        obj_data = jsonable_encoder(BookInstanceSchema.from_orm(db_obj))
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        for field in obj_data:
            if field in update_data:
                setattr(db_obj, field, update_data[field])
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj


class CRUDBookQueue(CRUDBase[BookQueue, BookQueueCreate, BookQueueUpdate]):
    def insert_into_end_of_book_queue(
        self, db: Session, *, obj_in: BookQueueCreate
    ) -> BookQueue:
        last_element_in_queue: BookQueue = (
            db.query(self.model)
            .where(
                (self.model.book_id == obj_in.book_id)
                & (self.model.next_id == None)  # noqa
            )
            .first()
        )

        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(
            **obj_in_data,  # noqa
            prev_id=last_element_in_queue.id if last_element_in_queue else None  # noqa
        )
        db.add(db_obj)
        db.flush()
        db.refresh(db_obj)

        if last_element_in_queue:
            last_element_in_queue.next_id = db_obj.id
            db.add(last_element_in_queue)
        db.commit()

        return db_obj

    def get_book_queue(self, db: Session, book_id: int) -> List[BookQueue]:
        non_recursive_term = (
            db.query(
                self.model.id,
                self.model.prev_id,
                self.model.next_id,
                self.model.user_id,
            )
            .where((self.model.book_id == book_id) & (self.model.prev_id == None))
            .cte("cte", recursive=True)
        )

        recursive_term = db.query(
            self.model.id, self.model.prev_id, self.model.next_id, self.model.user_id
        ).join(non_recursive_term, self.model.prev_id == non_recursive_term.c.id)

        recursive_query = non_recursive_term.union(recursive_term)

        return (
            db.query(User)
            .join(
                recursive_query,
                User.id == recursive_query.c.user_id,
            )
            .all()
        )

    def get_queue_record_by_user(
        self, db: Session, *, user_id: int, book_id: int
    ) -> Optional[BookQueue]:
        return (
            db.query(self.model)
            .where((self.model.user_id == user_id) & (self.model.book_id == book_id))
            .first()
        )

    def pop_by_book(self, db: Session, *, book_id: int) -> Optional[BookQueue]:
        queue = (
            db.query(self.model)
            .where((self.model.book_id == book_id) & (self.model.prev_id == None))
            .first()
        )

        if not queue:
            return

        db.delete(queue)
        db.commit()

        return queue

    def remove_from_book_queue(
        self, db: Session, *, obj_in: BookQueueUpdate
    ) -> BookQueue:
        queue_record: BookQueue = (
            db.query(self.model)
            .where(
                (self.model.book_id == obj_in.book_id)
                & (self.model.user_id == obj_in.user_id)
            )
            .first()
        )
        if queue_record.prev_id:
            previous_queue: BookQueue = db.query(self.model).get(queue_record.prev_id)
            previous_queue.next_id = queue_record.next_id
            db.add(previous_queue)

        if queue_record.next_id:
            next_queue: BookQueue = db.query(self.model).get(queue_record.next_id)
            next_queue.prev_id = queue_record.prev_id
            db.add(next_queue)

        db.delete(queue_record)
        db.commit()

        return queue_record

    def get_last_position_in_book_queue(self, db: Session, book_id: int) -> BookQueue:
        return db.query(self.model).where(self.model.book_id == book_id)


book = CRUDBook(Book)
book_instance = CRUDBookInstance(BookInstance)
book_queue = CRUDBookQueue(BookQueue)
