from sqlalchemy.orm import Session

from ..schemas.user import UserCreate, UserUpdate
from ..models import User
from .base import CRUDBase


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    def get_by_telegram_uid(self, session: Session, telegram_uid: str) -> User:
        return (
            session.query(self.model)
            .where(self.model.telegram_uid == telegram_uid)
            .first()
        )


user = CRUDUser(User)
