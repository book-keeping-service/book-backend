from typing import Optional, List

from pydantic import BaseModel

from .user import User
from ..models.book import StatusType


class BookBase(BaseModel):
    name: Optional[str] = None
    author: Optional[str] = None
    isbn: Optional[str] = None
    description: Optional[str] = None


class BookCreate(BookBase):
    isbn: str


class BookUpdate(BookBase):
    pass


class BookInDBBase(BookBase):
    id: int
    qty: int

    class Config:
        orm_mode = True


class Book(BookInDBBase):
    pass


class BookInDB(BookInDBBase):
    pass


class BookInstanceBase(BaseModel):
    status: Optional[StatusType] = StatusType.available
    owner_id: Optional[int] = None
    book_id: Optional[int] = None
    loaned_by: Optional[int] = None


class BookInstanceCreate(BookInstanceBase):
    owner_id: Optional[int] = None
    book_id: int


class BookInstanceUpdate(BookInstanceBase):
    pass


class BookInstanceInDBBase(BookInstanceBase):
    id: int

    class Config:
        orm_mode = True


class BookInstance(BookInstanceInDBBase):
    book: Book
    owner: User
    borrower: Optional[User] = None


class BookInstanceInDB(BookInstanceInDBBase):
    pass


class BookQueueBase(BaseModel):
    book_id: Optional[int] = None
    user_id: Optional[int] = None


class BookQueueCreate(BookQueueBase):
    book_id: int
    user_id: int


class BookQueueUpdate(BookQueueBase):
    book_id: int
    user_id: int


class BookQueueInDBBase(BookQueueBase):
    id: int

    class Config:
        orm_mode = True


class BookQueue(BookQueueInDBBase):
    user: User


class BookQueueInDB(BookQueueInDBBase):
    pass


class BookWithQueue(BaseModel):
    book_instance: Optional[BookInstance] = None
    queue: List[User]
