from typing import List

from pydantic import ValidationError
from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, Header

from book_backend.utils import get_session
from .. import crud
from ..schemas.user import UserCreate
from ..crud.user import user
from book_backend.models.user import User
from book_backend.schemas.user import User as UserSchema


class UserService:
    def __init__(self, session: Session = Depends(get_session)) -> None:
        self.session = session

    def get_all_users(self) -> List[User]:
        return crud.user.get_multi(self.session)

    def get_by_user_id(self, user_id: int) -> User:
        user_db: User = user.get(self.session, user_id)

        if not user_db:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

        return user_db

    def get_by_telegram_uid(self, telegram_uid: str) -> User:
        user_db: User = user.get_by_telegram_uid(self.session, telegram_uid)

        if not user_db:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

        return user_db

    def check_user_already_exists(self, telegram_uid: str) -> bool:
        user_db: User = user.get_by_telegram_uid(self.session, telegram_uid)

        return type(user_db) == User

    def register_new_user(self, user_data: UserCreate) -> User:
        if self.check_user_already_exists(user_data.telegram_uid):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="User with this telegram uid already exists",
            )

        created_user = user.create(self.session, obj_in=user_data)

        return created_user


def get_current_user(
    x_telegram_id: str = Header(None), service: UserService = Depends()
) -> UserSchema:
    """
    Временное решение аутентификации пользовотеля, позже переделаю на JWT
    """
    user_db = service.get_by_telegram_uid(x_telegram_id)

    try:
        user = UserSchema.from_orm(user_db)
    except ValidationError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
        )

    return user
