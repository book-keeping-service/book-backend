from typing import List
from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status

from book_backend.schemas.user import User
from book_backend.utils import get_session
from book_backend.schemas.book import (
    BookCreate,
    BookInstanceCreate,
    BookInstanceUpdate,
    BookQueueCreate,
    BookWithQueue,
    BookInstanceInDB as BookInstanceSchemaInDB,
)
from book_backend.models.book import Book, BookInstance, BookQueue, StatusType
from book_backend import crud


class BookService:
    def __init__(self, session: Session = Depends(get_session)) -> None:
        self.session = session

    def get_available_book_types(self) -> List[Book]:
        return crud.book.get_available_multi(self.session)

    def get_all_book_types(self) -> List[Book]:
        return crud.book.get_multi(self.session)

    def get_all_books(self) -> List[BookInstance]:
        return crud.book_instance.get_multi(self.session)

    def create_new_book_type(self, book_data: BookCreate) -> Book:
        book_db = crud.book.create(self.session, obj_in=book_data)

        return book_db

    def get_book_instance_info_by_id(self, book_id: int) -> BookInstance:
        book_db = crud.book_instance.get(self.session, book_id)

        if not book_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Could not find book with this id",
            )

        return book_db

    def add_new_book(self, book_data: BookInstanceCreate) -> BookInstance:
        book_db = crud.book_instance.create(self.session, obj_in=book_data)

        return book_db

    def get_available_books(self) -> List[BookInstance]:
        return crud.book_instance.get_available_books(self.session)

    def update_book_info(
        self, instance_id: int, book_data: BookInstanceUpdate
    ) -> BookInstance:
        book_db = crud.book_instance.get(self.session, instance_id)

        if not book_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Book with this id not found",
            )

        return crud.book_instance.update(self.session, db_obj=book_db, obj_in=book_data)

    def get_book_by_isbn(self, isbn: str) -> Book:
        book_type_db = crud.book.get_by_isbn(self.session, isbn)

        if not book_type_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Could not find book with this isbn",
            )

        return book_type_db

    def get_books_by_telegram_user(self, telegram_uid: str) -> List[BookInstance]:
        user_db = crud.user.get_by_telegram_uid(self.session, telegram_uid)

        if not user_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Could not find user with this telegram uid",
            )

        return crud.book_instance.get_books_by_user(self.session, user_db.id)

    def add_to_end_of_book_queue(self, queue_data: BookQueueCreate) -> List[BookQueue]:
        crud.book_queue.insert_into_end_of_book_queue(self.session, obj_in=queue_data)

        return crud.book_queue.get_book_queue(self.session, queue_data.book_id)

    def borrow_available_book(self, *, book: BookInstance, user: User) -> BookInstance:
        if book.status != StatusType.available or book.loaned_by:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Cannot borrow already borrowed book",
            )

        instance_update_data = BookInstanceUpdate(
            status=StatusType.loaned, loaned_by=user.id
        )

        return crud.book_instance.update(
            self.session, db_obj=book, obj_in=instance_update_data
        )

    def get_book_by_id(self, book_id: int) -> Book:
        book_db: Book = crud.book.get(self.session, book_id)

        if not book_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Could not find book with id {0}".format(book_id),
            )

        return book_db

    def proceed_borrowing_book(self, book_type_id: int, user: User) -> BookWithQueue:
        book_type_db = self.get_book_by_id(book_type_id)

        for book_instance in book_type_db.book_instances:
            if (
                book_instance.status == StatusType.available
                and not book_instance.loaned_by
            ):
                borrowed_book = self.borrow_available_book(
                    book=book_instance, user=user
                )
                return BookWithQueue(book_instance=borrowed_book, queue=[])

        # Check if user already in queue
        if crud.book_queue.get_queue_record_by_user(
            self.session, user_id=user.id, book_id=book_type_db.id
        ):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Could not register in queue already registered user",
            )

        # Register user in book queue
        book_queue = self.add_to_end_of_book_queue(
            queue_data=BookQueueCreate(book_id=book_type_db.id, user_id=user.id)
        )

        return BookWithQueue(queue=[User.from_orm(record) for record in book_queue])

    def proceed_returning_book(self, book_id: int) -> BookInstance:
        book_db = self.get_book_instance_info_by_id(book_id)
        book_data = BookInstanceSchemaInDB.from_orm(book_db)

        if book_db.book.queues:
            queue_record = crud.book_queue.pop_by_book(
                self.session, book_id=book_data.book_id
            )
            book_data.loaned_by = queue_record.user_id
        else:
            book_data.loaned_by = None
            book_data.status = StatusType.available

        return crud.book_instance.update(
            self.session,
            db_obj=book_db,
            obj_in=book_data,
        )

    def remove_book_instance(self, book_id: int):
        book_instance_db = crud.book_instance.get(self.session, book_id)

        if not book_instance_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Could not find book with id {0}".format(str(book_id)),
            )

        removed_book_instance = crud.book_instance.remove(self.session, id=book_id)
        book_type = crud.book.get(self.session, removed_book_instance.book_id)
        crud.book.update(
            self.session, db_obj=book_type, obj_in={"qty": book_type.qty - 1}
        )

        return crud.user.get(self.session, id=removed_book_instance.loaned_by)
