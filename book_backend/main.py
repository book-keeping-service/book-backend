from fastapi import FastAPI

from book_backend.api.api_v1 import router


app = FastAPI()

app.include_router(router)


@app.get("/ping")
def health_check():
    return {"ping": "pong"}
