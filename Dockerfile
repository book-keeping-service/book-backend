FROM python:3.9-buster

ARG PYTHON_ENV=development

ENV POETRY_VERSION=1.1.12 \
  POETRY_VIRTUALENVS_CREATE=false \
  SERVER_HOST=0.0.0.0 \
  SERVER_PORT=8000

RUN pip install "poetry==${POETRY_VERSION}"

WORKDIR /code

COPY ./poetry.lock ./pyproject.toml /code/

RUN poetry install $(test ${PYTHON_ENV} == production && echo "--no-dev")

COPY ./book_backend /code/book_backend

EXPOSE ${SERVER_PORT}

CMD ["poetry", "run", "start"]
