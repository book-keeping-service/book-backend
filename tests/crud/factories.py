"""
Factories of SQLAlchemy models
"""
import factory

from book_backend.models import BookInstance


class BookInstanceFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = BookInstance
