from book_backend import crud
from book_backend.schemas.book import (
    BookCreate,
    BookInstanceCreate,
    BookQueueCreate,
    BookQueueUpdate,
)
from book_backend.schemas.user import UserCreate


def test_get_by_isbn(db):
    book = crud.book.create(
        db,
        obj_in=BookCreate(name="Test book", author="Test author", isbn="1234567890123"),
    )
    another_book = crud.book.create(
        db,
        obj_in=BookCreate(
            name="Another book", author="Another author", isbn="0000000000000"
        ),
    )

    founded_book = crud.book.get_by_isbn(db, isbn=book.isbn)

    assert founded_book == book


def test_create_book_instance(db):
    book = crud.book.create(
        db,
        obj_in=BookCreate(name="Test book", author="Test author", isbn="1234567890123"),
    )
    user = crud.user.create(db, obj_in=UserCreate(name="user1", telegram_uid="uid1"))

    book_instance = crud.book_instance.create(
        db, obj_in=BookInstanceCreate(owner_id=user.id, book_id=book.id)
    )
    db.refresh(book)

    assert (book_instance.id, book_instance.book, book_instance.owner) == (
        1,
        book,
        user,
    )
    assert book.qty == 1


def test_create_queue(db):
    book = crud.book.create(
        db,
        obj_in=BookCreate(name="Test book", author="Test author", isbn="1234567890123"),
    )
    book_owner = crud.user.create(
        db, obj_in=UserCreate(name="user1", telegram_uid="uid1")
    )
    user2 = crud.user.create(db, obj_in=UserCreate(name="user2", telegram_uid="uid2"))
    user3 = crud.user.create(db, obj_in=UserCreate(name="user3", telegram_uid="uid3"))
    book_instance = crud.book_instance.create(
        db, obj_in=BookInstanceCreate(owner_id=book_owner.id, book_id=book.id)
    )

    queue_record_1 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user2.id)
    )
    queue_record_2 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user3.id)
    )

    assert queue_record_1.book == book
    assert queue_record_1.user_id == user2.id
    assert queue_record_1.next_id == queue_record_2.id
    assert queue_record_1.prev_id == None  # noqa
    assert queue_record_2.book == book
    assert queue_record_2.user_id == user3.id
    assert queue_record_2.prev_id == queue_record_1.id
    assert queue_record_2.next_id == None  # noqa


def test_delete_last_item_from_queue(db):
    book = crud.book.create(
        db,
        obj_in=BookCreate(name="Test book", author="Test author", isbn="1234567890123"),
    )
    book_owner = crud.user.create(
        db, obj_in=UserCreate(name="user1", telegram_uid="uid1")
    )
    user2 = crud.user.create(db, obj_in=UserCreate(name="user2", telegram_uid="uid2"))
    user3 = crud.user.create(db, obj_in=UserCreate(name="user3", telegram_uid="uid3"))
    book_instance = crud.book_instance.create(
        db, obj_in=BookInstanceCreate(owner_id=book_owner.id, book_id=book.id)
    )
    queue_record_1 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user2.id)
    )
    queue_record_2 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user3.id)
    )

    removed_queue_record = crud.book_queue.remove_from_book_queue(
        db, obj_in=BookQueueUpdate(book_id=book.id, user_id=user3.id)
    )
    db.refresh(queue_record_1)

    assert removed_queue_record == queue_record_2
    assert queue_record_1.next_id == None  # noqa


def test_delete_first_item_from_queue(db):
    book = crud.book.create(
        db,
        obj_in=BookCreate(name="Test book", author="Test author", isbn="1234567890123"),
    )
    book_owner = crud.user.create(
        db, obj_in=UserCreate(name="user1", telegram_uid="uid1")
    )
    user2 = crud.user.create(db, obj_in=UserCreate(name="user2", telegram_uid="uid2"))
    user3 = crud.user.create(db, obj_in=UserCreate(name="user3", telegram_uid="uid3"))
    book_instance = crud.book_instance.create(
        db, obj_in=BookInstanceCreate(owner_id=book_owner.id, book_id=book.id)
    )
    queue_record_1 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user2.id)
    )
    queue_record_2 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user3.id)
    )

    removed_queue_record = crud.book_queue.remove_from_book_queue(
        db, obj_in=BookQueueUpdate(book_id=book.id, user_id=user2.id)
    )
    db.refresh(queue_record_2)

    assert removed_queue_record == queue_record_1
    assert queue_record_2.prev_id == None  # noqa


def test_delete_item_in_center_from_queue(db):
    book = crud.book.create(
        db,
        obj_in=BookCreate(name="Test book", author="Test author", isbn="1234567890123"),
    )
    book_owner = crud.user.create(
        db, obj_in=UserCreate(name="user1", telegram_uid="uid1")
    )
    user2 = crud.user.create(db, obj_in=UserCreate(name="user2", telegram_uid="uid2"))
    user3 = crud.user.create(db, obj_in=UserCreate(name="user3", telegram_uid="uid3"))
    user4 = crud.user.create(db, obj_in=UserCreate(name="user4", telegram_uid="uid4"))
    book_instance = crud.book_instance.create(
        db, obj_in=BookInstanceCreate(owner_id=book_owner.id, book_id=book.id)
    )
    queue_record_1 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user2.id)
    )
    queue_record_2 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user3.id)
    )
    queue_record_3 = crud.book_queue.insert_into_end_of_book_queue(
        db, obj_in=BookQueueCreate(book_id=book.id, user_id=user4.id)
    )

    removed_queue_record = crud.book_queue.remove_from_book_queue(
        db, obj_in=BookQueueUpdate(book_id=book.id, user_id=user3.id)
    )
    db.refresh(queue_record_1)
    db.refresh(queue_record_3)

    assert removed_queue_record == queue_record_2
    assert queue_record_1.next_id == queue_record_3.id
    assert queue_record_3.prev_id == queue_record_1.id


def test_get_available_book_type(db):
    book = crud.book.create(
        db,
        obj_in=BookCreate(name="Test book", author="Test author", isbn="1234567890123"),
    )
    book_owner = crud.user.create(
        db, obj_in=UserCreate(name="user1", telegram_uid="uid1")
    )
    book_instance = crud.book_instance.create(
        db, obj_in=BookInstanceCreate(owner_id=book_owner.id, book_id=book.id)
    )
    book_without_instances = crud.book.create(
        db,
        obj_in=BookCreate(name="Test book", author="Test author", isbn="1111111"),
    )

    available_books = crud.book.get_available_multi(db)

    assert available_books == [book]
