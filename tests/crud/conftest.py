import pytest
from alembic.command import upgrade
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from yarl import URL

from book_backend.utils import tmp_database, alembic_config_from_url


@pytest.fixture(scope="session")
def migrated_postgres_template(pg_url):
    """
    Creates temporary database and applies migrations.
    Database can be used as template to fast creation databases for tests.
    Has "session" scope, so is called only once per tests run.
    """
    with tmp_database(pg_url, "template") as tmp_url:
        alembic_config = alembic_config_from_url(tmp_url)
        upgrade(alembic_config, "head")
        yield tmp_url


@pytest.fixture
def migrated_postgres(pg_url, migrated_postgres_template):
    """
    Quickly creates clean migrated database using temporary database as base.
    Use this fixture in tests that require migrated database.
    """
    template_db = URL(migrated_postgres_template).name
    with tmp_database(pg_url, "pytest", template=template_db) as tmp_url:
        yield tmp_url


@pytest.fixture
def db(migrated_postgres):
    engine = create_engine(migrated_postgres, pool_pre_ping=True)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()
    try:
        yield session
    finally:
        session.close()
