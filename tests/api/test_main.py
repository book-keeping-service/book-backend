from starlette import status


def test_ping(api_client):
    """
    Testing that app is executable without any errors
    """
    health_check_stub = {"ping": "pong"}

    response = api_client.get("/ping")

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == health_check_stub
