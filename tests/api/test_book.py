import pytest
from starlette import status

from book_backend.schemas.book import BookCreate, Book, BookInstanceCreate, BookInstance
from book_backend.schemas.user import UserCreate, User


@pytest.fixture
def book_type_to_create():
    return BookCreate(
        name="Test name",
        author="Test author",
        description="Test description",
        isbn="1234567890123",
    )


@pytest.fixture
def user_to_create():
    return UserCreate(name="Test user", telegram_uid="12345678")


def test_create_book_type(api_client, book_type_to_create):
    response = api_client.post("/references/book/", json=book_type_to_create.dict())

    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == {"id": 1, "qty": 0, **book_type_to_create.dict()}


def test_create_book_instance(api_client, book_type_to_create, user_to_create):
    user = User.parse_obj(api_client.post("/users/", json=user_to_create.dict()).json())
    book = Book.parse_obj(
        api_client.post("/references/book/", json=book_type_to_create.dict()).json()
    )
    book_instance_to_create = BookInstanceCreate(book_id=book.id, owner_id=user.id)

    response = api_client.post("/books/", json=book_instance_to_create.dict())

    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == {
        "status": "Доступна",
        "owner_id": 1,
        "book_id": 1,
        "loaned_by": None,
        "id": 1,
        "book": {
            "name": "Test name",
            "author": "Test author",
            "isbn": "1234567890123",
            "description": "Test description",
            "id": 1,
            "qty": 1,
        },
        "owner": {"name": "Test user", "telegram_uid": "12345678", "id": 1},
        "borrower": None,
    }


def test_borrow_multiply_book_instances(api_client, faker, book_type):
    owner1 = User.parse_obj(
        api_client.post(
            "/users/",
            json=UserCreate(
                name=faker.name(), telegram_uid=faker.random_number()
            ).dict(),
        ).json()
    )
    owner2 = User.parse_obj(
        api_client.post(
            "/users/",
            json=UserCreate(
                name=faker.name(), telegram_uid=faker.random_number()
            ).dict(),
        ).json()
    )
    user1 = User.parse_obj(
        api_client.post(
            "/users/",
            json=UserCreate(
                name=faker.name(), telegram_uid=faker.random_number()
            ).dict(),
        ).json()
    )
    user2 = User.parse_obj(
        api_client.post(
            "/users/",
            json=UserCreate(
                name=faker.name(), telegram_uid=faker.random_number()
            ).dict(),
        ).json()
    )
    user3 = User.parse_obj(
        api_client.post(
            "/users/",
            json=UserCreate(
                name=faker.name(), telegram_uid=faker.random_number()
            ).dict(),
        ).json()
    )
    book = Book.parse_obj(
        api_client.post("/references/book/", json=book_type.dict()).json()
    )
    book_instance_1 = BookInstance.parse_obj(
        api_client.post(
            "/books/",
            json=BookInstanceCreate(book_id=book.id, owner_id=owner1.id).dict(),
        ).json()
    )
    book_instance_2 = BookInstance.parse_obj(
        api_client.post(
            "/books/",
            json=BookInstanceCreate(book_id=book.id, owner_id=owner2.id).dict(),
        ).json()
    )
    api_client.post(
        f"/references/book/{book.id}/borrow",
        headers={"x-telegram-id": user1.telegram_uid},
    )
    api_client.post(
        f"/references/book/{book.id}/borrow",
        headers={"x-telegram-id": user2.telegram_uid},
    )
    api_client.post(
        f"/references/book/{book.id}/borrow",
        headers={"x-telegram-id": user3.telegram_uid},
    )
    response = BookInstance.parse_obj(
        api_client.patch(f"/books/{book_instance_1.id}").json()
    )

    assert response.id == book_instance_1.id
    assert response.borrower.id == user3.id


def test_get_all_user_books(api_client, faker, book_type):
    book = Book.parse_obj(
        api_client.post("/references/book/", json=book_type.dict()).json()
    )
    owner_1 = User.parse_obj(
        api_client.post(
            "/users/",
            json=UserCreate(
                name=faker.name(), telegram_uid=faker.random_number()
            ).dict(),
        ).json()
    )
    owner_2 = User.parse_obj(
        api_client.post(
            "/users/",
            json=UserCreate(
                name=faker.name(), telegram_uid=faker.random_number()
            ).dict(),
        ).json()
    )
    book_instance_1 = BookInstance.parse_obj(
        api_client.post(
            "/books/",
            json=BookInstanceCreate(book_id=book.id, owner_id=owner_1.id).dict(),
        ).json()
    )
    book_instance_2 = BookInstance.parse_obj(
        api_client.post(
            "/books/",
            json=BookInstanceCreate(book_id=book.id, owner_id=owner_2.id).dict(),
        ).json()
    )
    api_client.post(
        f"/references/book/{book.id}/borrow",
        headers={"x-telegram-id": owner_2.telegram_uid},
    )

    response = [
        BookInstance.parse_obj(book_instance)
        for book_instance in api_client.get(
            "/books/?telegram_uid={0}".format(owner_2.telegram_uid)
        ).json()
    ]
    book_instance_1 = BookInstance.parse_obj(
        api_client.get(f"/books/{str(book_instance_1.id)}").json()
    )

    assert [book_instance_2, book_instance_1] == response
