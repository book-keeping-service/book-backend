from starlette import status

from book_backend import crud

USER = {"name": "John", "telegram_uid": "12345678"}


def test_create(api_client, db):
    response = api_client.post("/users/", json=USER)

    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == {"id": 1, "name": "John", "telegram_uid": "12345678"}
    assert len(crud.user.get_multi(db)) == 1


def test_not_create_existing_user(api_client):
    api_client.post("/users/", json=USER)

    response = api_client.post("/users/", json=USER)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
