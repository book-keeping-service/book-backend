# Book backend
Приложение представляет собой микросервис для учета книг

## Запуск проекта
1. Создать пустую бд с именем book в PostgreSQL
2. Установить [poetry](https://python-poetry.org/docs/#installation)
3. poetry install
4. poetry run migrate
5. poetry run start
